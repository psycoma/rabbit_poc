package service;

import bean.Seat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ModelContainer {
    List<Seat> model = new ArrayList<Seat>();

    public void addSeat(Seat seat){
        model.add(seat);
    }

    public List<Seat> getModel() {
        return model;
    }

    public void setModel(List<Seat> model) {
        this.model = model;
    }
}

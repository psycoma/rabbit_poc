package service;

import atmosphere.ModelChangeBroadcastService;
import bean.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class SeatAllocation {
    @Autowired
    private ModelContainer modelContainer;

    @Autowired
    private ModelChangeBroadcastService modelChangeBroadcastService;

    @PostConstruct
    public void init(){
        modelContainer.addSeat(new Seat("first"));
        modelContainer.addSeat(new Seat("second"));
    }
    public void processSeat(Seat seat){
        modelContainer.addSeat(seat);
        modelChangeBroadcastService.broadcast();
    }

    public ModelContainer getModel(){
        return modelContainer;
    }
}

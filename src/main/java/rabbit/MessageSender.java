package rabbit;

import bean.Seat;
import configuration.Configuration;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void sendSeat(Seat seat){
        rabbitTemplate.convertAndSend(Configuration.queueName, seat);
    }

}

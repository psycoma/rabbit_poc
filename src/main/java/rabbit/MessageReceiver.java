package rabbit;

import bean.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.SeatAllocation;

@Component
public class MessageReceiver {

    @Autowired
    SeatAllocation seatAllocation;

    public void receiveMessage(Seat seat){
        seatAllocation.processSeat(seat);
    }
}

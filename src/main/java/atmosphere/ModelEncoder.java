package atmosphere;


import org.atmosphere.config.managed.Encoder;
import org.codehaus.jackson.map.ObjectMapper;
import service.ModelContainer;

import java.io.IOException;

public class ModelEncoder implements Encoder<ModelContainer, String> {
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public String encode(ModelContainer s) {
        try {
            return mapper.writeValueAsString(s);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

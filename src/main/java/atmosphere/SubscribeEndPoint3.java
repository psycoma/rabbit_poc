package atmosphere;


import org.atmosphere.annotation.Broadcast;
import org.atmosphere.annotation.Suspend;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/*
@Path("/subscribe/")
@AtmosphereService(broadcaster = JerseyBroadcaster.class)*/
public class SubscribeEndPoint3 {

    @Suspend
    @GET
    public Response subscribe() {
        System.out.println("here");
        return null;
    }

    @Broadcast(writeEntity = false)
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response broadcast(String message) {
        return Response.ok().entity(message).build();
    }
}

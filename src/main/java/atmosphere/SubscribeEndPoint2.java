package atmosphere;


import org.atmosphere.cpr.AtmosphereHandler;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.cpr.Broadcaster;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/*@AtmosphereHandlerService(path = "/subscribe",
        broadcasterCache = UUIDBroadcasterCache.class,
        interceptors = { AtmosphereResourceLifecycleInterceptor.class,
                         //BroadcastOnPostAtmosphereInterceptor.class,
                         TrackMessageSizeInterceptor.class,
                         HeartbeatInterceptor.class
        })*/
public class SubscribeEndPoint2 implements AtmosphereHandler {

    @Override
    public void onRequest(AtmosphereResource atmosphereResource) throws IOException {
        if (atmosphereResource.getRequest().getMethod().equalsIgnoreCase("GET")) {
            atmosphereResource.suspend();
        } else if (atmosphereResource.getRequest().getMethod().equalsIgnoreCase("POST")) {
            Broadcaster broadcaster = atmosphereResource.getBroadcaster();
            broadcaster.broadcast(atmosphereResource.getRequest().getReader().readLine().trim());
        }
    }

    @Override
    public void onStateChange(AtmosphereResourceEvent atmosphereResourceEvent) throws IOException {
        AtmosphereResource resource = atmosphereResourceEvent.getResource();
        AtmosphereResponse response = resource.getResponse();
        ObjectMapper mapper = new ObjectMapper();

        if (resource.isSuspended()) {
            String body = atmosphereResourceEvent.getMessage().toString();
            String author = body.substring(body.indexOf(":") + 2, body.indexOf(",") - 1);
            String message = body.substring(body.lastIndexOf(":") + 2, body.length() - 2);
            response.getWriter().write(body);
            response.getWriter().flush();
        }
    }

    @Override
    public void destroy() {

    }
}

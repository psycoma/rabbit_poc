package atmosphere;


import bean.Seat;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Heartbeat;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import service.ModelContainer;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static org.atmosphere.cpr.ApplicationConfig.MAX_INACTIVE;


@ManagedService(path = "/subscribe", atmosphereConfig = MAX_INACTIVE + "=120000")
public class SubscribeEndPoint {

    @Autowired
    ModelChangeBroadcastService modelChangeBroadcastService;

    private final Logger logger = LoggerFactory.getLogger(SubscribeEndPoint.class);

    @Heartbeat
    public void onHeartbeat(final AtmosphereResourceEvent event) {
        logger.info("Heartbeat send by {}", event.getResource());
    }

    @Ready
    public void onReady(final AtmosphereResource resource) {
        String channelId = resource.getRequest().getParameter("channelId");
        Broadcaster broadcaster = resource.getAtmosphereConfig().getBroadcasterFactory().lookup(channelId, true);
        broadcaster.addAtmosphereResource(resource);

        modelChangeBroadcastService.addBroadcastToken(channelId, broadcaster);
    }

    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        String channelId = event.getResource().getRequest().getParameter("channelId");
        modelChangeBroadcastService.destroyBroadcastToken(channelId);
    }

    @org.atmosphere.config.service.Message(encoders = {ModelEncoder.class})
    public ModelContainer onMessage(ModelContainer modelContainer) throws IOException {
        return modelContainer;
    }


}

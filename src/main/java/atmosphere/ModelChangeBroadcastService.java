package atmosphere;

import service.SeatAllocation;
import org.atmosphere.cpr.Broadcaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ModelChangeBroadcastService {
    private Map<String, Broadcaster> broadcastTokens = new ConcurrentHashMap<String, Broadcaster>();

    @Autowired
    private SeatAllocation seatAllocation;

    public void broadcast() {
        for (Broadcaster token : broadcastTokens.values()) {
            token.broadcast(seatAllocation.getModel());
        }
    }

    public void addBroadcastToken(String channel, Broadcaster token) {
        broadcastTokens.put(channel, token);
    }

    public void destroyBroadcastToken(String channel) {
        Broadcaster token = broadcastTokens.get(channel);
        if (token != null) {
            token.destroy();
            broadcastTokens.remove(channel);
        }
    }
}

package rest;


import atmosphere.ModelChangeBroadcastService;
import bean.Seat;
import rabbit.MessageSender;
import service.SeatAllocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Service
@Path("/seat")
@Produces("application/json")
public class SeatEndPoint {

    @Autowired
    private SeatAllocation seatAllocation;

    @Autowired
    private MessageSender messageSender;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getModel() {
        return Response.ok().entity(seatAllocation.getModel()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void submitSeat(Seat seat) {
        try{
            messageSender.sendSeat(seat);
        } catch (Exception e) {
            seatAllocation.processSeat(seat);
        }
    }

}
